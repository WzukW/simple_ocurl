(*************************************************************************************)
(* Copyright © zbroyar, 2011                                                         *)
(* Copyright © Joly Clément, 2015                                                    *)
(*                                                                                   *)
(* leowzukw@vmail.me                                                                 *)
(*                                                                                   *)
(* This software is a computer program whose purpose is to pass http requests.       *)
(*                                                                                   *)
(* This software is governed by the CeCILL-C license under French                    *)
(* law and abiding by the rules of distribution of free software.  You can  use,     *)
(* modify and/ or redistribute the software under the terms of the                   *)
(* CeCILL-C license as circulated by CEA, CNRS and INRIA at the                      *)
(* following URL "http://www.cecill.info".                                           *)
(*                                                                                   *)
(* As a counterpart to the access to the source code and  rights to copy, modify     *)
(* and redistribute granted by the license, users are provided only with a limited   *)
(* warranty  and the software's author,  the holder of the economic rights,  and     *)
(* the successive licensors  have only  limited liability.                           *)
(*                                                                                   *)
(* In this respect, the user's attention is drawn to the risks associated with       *)
(* loading,  using,  modifying and/or developing or reproducing the software by the  *)
(* user in light of its specific status of free software, that may mean  that it is  *)
(* complicated to manipulate,  and  that  also therefore means  that it is reserved  *)
(* for developers  and  experienced professionals having in-depth computer           *)
(* knowledge. Users are therefore encouraged to load and test the software's         *)
(* suitability as regards their requirements in conditions enabling the security of  *)
(* their systems and/or data to be ensured and,  more generally, to use and operate  *)
(* it in the same conditions as regards security.                                    *)
(*                                                                                   *)
(* The fact that you are presently reading this means that you have had knowledge    *)
(* of the CeCILL-C license and that you accept its terms.                            *)
(*                                                                                   *)
(*************************************************************************************)


open Core.Std;;

(** A little binary that uses the lib *)

module Sc= Simple_curl;;

(* Function to display the result *)
let disp result =
    printf "%s\nWith code: %i" result.Sc.content result.Sc.rc
;;

let def_cmd summary func =
    Command.basic ~summary Command.Spec.(
    empty
    +> anon ("url" %: string)
    +> anon (maybe_with_default "" ("user agent" %: string))
    )
    func

(* Use function of the library *)
let get =
    def_cmd "Get"
    (fun url user_agent () ->
        Sc.get url
        |> disp)
;;

let post =
    def_cmd "Post, read data from stdin"
    (fun url user_agent () ->
        Sc.post ~data:(In_channel.input_all In_channel.stdin) url
        |> disp)
;;

let put =
    def_cmd "Put, read data from stdin"
    (fun url user_agent () ->
        Sc.put ~data:(In_channel.input_all In_channel.stdin) url
        |> disp)
;;

let delete =
    def_cmd "Delete"
    (fun url user_agent () ->
        Sc.delete url
        |> disp)
;;


let command = Command.group ~summary:"Some commands to test the library"
    ["get", get ; "post", post ; "put", put ; "delete", delete ]

let () =
    Command.run ~version:"0.1" ~build_info:"" command;
;;
