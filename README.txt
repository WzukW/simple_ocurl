(* OASIS_START *)
(* DO NOT EDIT (digest: 97c697cd547c4f57a841ff401bceec51) *)

simple_curl - Simple lib to use curl
====================================



See the file [INSTALL.txt](INSTALL.txt) for building and installation
instructions.

[Home page](http://www.simple_curl.tuxfamily.org)

Copyright and license
---------------------

(C) 2011 zbroyar
(C) 2015 Joly Clément

simple_curl is distributed under the terms of the CEA-CNRS-INRIA Logiciel
Libre, LGPL-like.

See [LICENSE](LICENSE) for more information.

(* OASIS_STOP *)
